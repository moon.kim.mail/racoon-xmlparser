# racoon-xmlparser

Simple python script for parsing the xml-file (Racoon Mint) and exporting anonymized data of "digitale Stanzen". Anonymization is accomplished using sha256 hash. 
When using the python script version - sha256 hashing will be executed two times with added random_characters.

## Getting started

We recommend running the script in a virtual env. e.g.:
```
git clone https://gitlab.com/moon.kim.mail/racoon-xmlparser.git

cd racoon-xmlparser

python3 -m venv .venv    # create venv in the current directory ./env
pip install --upgrade pip    # upgrade pip - solves lots of problems beforehand
source .venv/bin/activate  # activate 

pip install -r requirements.txt

```
## Running the script

For simplicity reason the script scans its current working directory and takes the first .xml file found for xml-parsing. 

Simply run the script after ensuring that only one .xml file is in the same folder:
```
python xmlparser.py
```

The script is tested on ubuntu 20.04 LTS.

## xmlparser.exe 

xmlparser.exe is compiled using pyinstaller on windows 10 anaconda environment. To run the script simply copy the .xml-file and the xmlparser.exe in an empty folder and execute. The script takes 1-2 minutes to load.

## License

This script is provided under the terms of the MIT License (MIT)

Copyright (c) 2010 New York University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


